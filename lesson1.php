<?php
namespace lesson1;

class User
{
	private $name;
	private $balance;
	
	function __construct($n,$b=0)
	{
		$this->name=$n;
		$this->balance=$b;
	}
	
	private function setName($n)
	{
		$this->name=$n;
	}
	public function getName()
	{
		return $this->name;
	}
	
	protected function setBalance($b)
	{
		$this->balance=$b;
	}
	private function getBalance()
	{
		return $this->balance;
	}

	function printStatus()
	{
		return "У пользователя ".$this->getName()." сейчас на счету $".$this->getBalance();
	}
	
	function giveMoney($komu, $amount)
	{
		if ($this->getBalance()>=$amount)
		{
			$komu->setBalance($komu->getBalance()+$amount);
			$this->setBalance($this->getBalance()-$amount);
			return "Пользователь ".$this->getName()." перечислил $".$amount." пользователю ".$komu->getName();
		}
		else
		{
			return "У Вас недостаточно средств!!! Попробуйте перевести суму по меньше...";
		}
	}
}

//=============================================== ТЕСТЫ ===================================


$user1=new User('User1',15);
$user2=new User('User2',25);

echo $user1->printStatus()."<br />";
echo $user2->printStatus()."<br />";

echo $user1->giveMoney($user2,16)."<br />";
echo $user1->printStatus()."<br />";
echo $user2->printStatus()."<br />";

echo $user1->giveMoney($user2,14)."<br />";
echo $user1->printStatus()."<br />";
echo $user2->printStatus()."<br />";

echo $user2->giveMoney($user1,30)."<br />";
echo $user1->printStatus()."<br />";
echo $user2->printStatus()."<br />";

?>