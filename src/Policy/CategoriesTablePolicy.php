<?php
declare(strict_types=1);

namespace App\Policy;

use App\Model\Entity\Category;
use App\Model\Table\CategoriesTable;
use Authorization\IdentityInterface;

/**
 * Categories policy
 */
class CategoriesTablePolicy
{
    public function scopeIndex($user, $query)
    {
        if ($user->permissions == 'User')
            $query->where(['categories.deleted is null']);
        return $query;
    }

}
