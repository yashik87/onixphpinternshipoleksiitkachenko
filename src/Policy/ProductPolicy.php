<?php
declare(strict_types=1);

namespace App\Policy;

use App\Model\Entity\Product;
use Authorization\IdentityInterface;

/**
 * Product policy
 */
class ProductPolicy
{
    /**
     * Check if $user can create Product
     *
     * @param \Authorization\IdentityInterface $user The user.
     * @param \App\Model\Entity\Product $product
     * @return bool
     */
    public function canCreate(IdentityInterface $user, Product $product)
    {
        return ($user->permissions=='User');
    }

    public function canAdd(IdentityInterface $user, Product $product)
    {
        return ($user->permissions=='User');
    }

    /**
     * Check if $user can update Product
     *
     * @param \Authorization\IdentityInterface $user The user.
     * @param \App\Model\Entity\Product $product
     * @return bool
     */
    public function canUpdate(IdentityInterface $user, Product $product)
    {
        return ($user->id==$product->seller_user_id);
    }

    /**
     * Check if $user can delete Product
     *
     * @param \Authorization\IdentityInterface $user The user.
     * @param \App\Model\Entity\Product $product
     * @return bool
     */
    public function canDelete(IdentityInterface $user, Product $product)
    {
        return ($user->id==$product->seller_user_id);
    }

    /**
     * Check if $user can view Product
     *
     * @param \Authorization\IdentityInterface $user The user.
     * @param \App\Model\Entity\Product $product
     * @return bool
     */
//    public function canView(IdentityInterface $user, Product $product)
//    {
//    }
}
