<?php
declare(strict_types=1);

namespace App\Policy;

use App\Model\Entity\User;
use App\Model\Table\UsersTable;
use Authorization\IdentityInterface;

/**
 * Users policy
 */
class UsersTablePolicy
{
    public function canIndex(IdentityInterface $user, $query)
    {
        return ($user->permissions == 'Admin');
    }

}
