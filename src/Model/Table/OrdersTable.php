<?php
declare(strict_types=1);

namespace App\Model\Table;

use Authentication\Controller\Component\AuthenticationComponent;
use Cake\Http\Exception\BadRequestException;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Orders Model
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 * @property \App\Model\Table\HistoriesTable&\Cake\ORM\Association\HasMany $Histories
 * @property \App\Model\Table\OrdersProductsTable&\Cake\ORM\Association\HasMany $OrdersProducts
 * @property \App\Model\Table\ProductsTable $Products
 * @property AuthenticationComponent $Authentication
 * @method \App\Model\Entity\Order newEmptyEntity()
 * @method \App\Model\Entity\Order newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Order[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Order get($primaryKey, $options = [])
 * @method \App\Model\Entity\Order findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Order patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Order[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Order|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Order saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Order[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Order[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Order[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Order[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class OrdersTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('orders');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER',
        ]);
        $this->hasMany('Histories', [
            'foreignKey' => 'order_id',
        ]);
        $this->hasMany('OrdersProducts', [
            'foreignKey' => 'order_id',
        ]);
        $this->belongsToMany('Products', [
            'through' => 'OrdersProducts',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->dateTime('payed')
            ->allowEmptyDateTime('payed');

        $validator
            ->dateTime('deleted')
            ->allowEmptyDateTime('deleted');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'), ['errorField' => 'user_id']);

        return $rules;
    }

    public function addNewOrder($userId)
    {
        $activeOrder=$this->getActiveOrder($userId);
        if (!$activeOrder)
        {
            $activeOrder = $this->newEntity(['user_id'=>$userId]);
            if (!$this->save($activeOrder)) {
                throw new BadRequestException('Not added');
            }
        }
        return $activeOrder;
    }

    public function getActiveOrder($userId)
    {
        return $this->find()->contain(['Products'])->where(['user_id'=>$userId,'payed is NULL'])->first();
    }

    public function deleteOrder($userId)
    {
        $order = $this->get($this->getActiveOrder($userId)->id);
        if ($order->payed !== null)
            throw new BadRequestException('You can\'t delete this order, because it\'s payed');

        if (!$this->delete($order))
            throw new BadRequestException('Not deleted');
    }

    public function addOrder($productId,$userId)
    {
        $activeOrder=$this->addNewOrder($userId);
        $this->OrdersProducts->addProductToOrder($activeOrder->id,$productId);
        return $this->getActiveOrder($userId);
    }

    public function orderAmount($orderId)
    {
        $amount=0;
        foreach($this->productsOfOrder($orderId) as $product)
            $amount+=$product->price;
        if ($amount==0)
            throw new BadRequestException('Please add products to your order');
        return round($amount,2);
    }

    public function orderProductsIds($orderId)
    {
        foreach($this->productsOfOrder($orderId) as $product)
                $ids[]=$product->id;
        return $ids;
    }

    public function productsOfOrder($orderId)    {
        return $this->get($orderId, ['contain' => ['Products'] ])->products;
    }

    public function isAnoughtMoneyToPay($userId,$orderId)
    {
        $sum=$this->orderAmount($orderId);
        $currentUser = $this->Users->get($userId);
        if (floatval($currentUser->amount)<$sum)
            throw new BadRequestException('Not enough money');
        return true;
    }

    public function isSoldProductInOrder($orderId)
    {
        $products=$this->productsOfOrder($orderId);
        return in_array('Sold',array_column($products, 'status'));
    }

    public function moneyTransfer($orderId,$userId)
    {
        $fullSiteEarning=0;
        foreach ($this->productsOfOrder($orderId) as $value)
        {
            $siteEarning=round($value->price*0.05,2);
            $sellerEarning=$value->price-$siteEarning;
            $fullSiteEarning+=$siteEarning;

            $this->Users->changeAmount(($sellerEarning),$value->seller_user_id);
            $this->Histories->addHistory('SELL',$value['seller_user_id'],$orderId,($sellerEarning));
        }

        $this->Users->changeAmount($fullSiteEarning,-1000);
        $this->Histories->addHistory('EARNINGS',-1000,$orderId,$fullSiteEarning);

        $this->Users->changeAmount(($this->orderAmount($orderId)*(-1)),$userId);
        $this->Histories->addHistory('BUY',$userId,$orderId,($this->orderAmount($orderId)*(-1)));
    }

    public function payOrder($userId)
    {
        // ПЕРЕД ОПЛАТОЙ Я БЫ СДЕЛАЛ БРОНИРОВКУ ТОВАРА(на время необходимое для платежной системы)
        // - мало ли кто его удалит или купит - но нет времени переделывать:)))
        $this->getConnection()->transactional(function () use ($userId)
        {
            $order=$this->getActiveOrder($userId);
            if (!$order){
                throw new BadRequestException('You have not active order');
            }

            if ($this->isSoldProductInOrder($order->id)){
                throw new BadRequestException('Some products already sold');
            }

            if (!$this->isAnoughtMoneyToPay($userId,$order->id)){
                throw new BadRequestException('You have not enough money');
            }

            if (!$this->Products->setProductsSold($this->orderProductsIds($order->id), $userId))
                throw new BadRequestException('Something goes bad while saving products');

            $this->moneyTransfer($order->id,$userId);

            $order=$this->patchEntity($order,['payed'=>strval(time()),'id'=>strval($order->id)]);
            $this->save($order);
        });
    }

    public function deleteProduct($productId,$userId)
    {
        $activeOrder=$this->getActiveOrder($userId);
        if (!$activeOrder)
        {
            throw new BadRequestException('You have not any orders');
        }
        else
        {
            $this->OrdersProducts->deleteProductFromOrder($activeOrder->id,$productId);
        }
    }
}
