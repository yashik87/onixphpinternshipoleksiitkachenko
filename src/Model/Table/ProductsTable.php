<?php
declare(strict_types=1);

namespace App\Model\Table;

use Authentication\Controller\Component\AuthenticationComponent;
use Cake\Database\Expression\QueryExpression;
use Cake\Http\Exception\BadRequestException;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Products Model
 * @property AuthenticationComponent $Authentication
 * @property \App\Model\Table\CategoriesTable&\Cake\ORM\Association\BelongsTo $Categories
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 *
 * @method \App\Model\Entity\Product newEmptyEntity()
 * @method \App\Model\Entity\Product newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Product[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Product get($primaryKey, $options = [])
 * @method \App\Model\Entity\Product findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Product patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Product[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Product|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Product saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Product[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Product[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Product[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Product[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ProductsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     * @mixin \Search\Model\Behavior\SearchBehavior
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->addBehavior('Search.Search');

        $this->searchManager()
            ->value('category_id')
            ->add('name', 'Search.Like', [
                'before' => true,
                'after' => true,
                'mode' => 'or',
                'comparison' => 'LIKE',
                'wildcardAny' => '*',
                'wildcardOne' => '?',
                'fields' => ['name'],
            ])
            ->add('description', 'Search.Like', [
                'before' => true,
                'after' => true,
                'mode' => 'or',
                'comparison' => 'LIKE',
                'wildcardAny' => '*',
                'wildcardOne' => '?',
                'fields' => ['description'],
            ])
            ->value('status')
            ->value('price')
            ->value('created')
            ->value('seller_user_id')
            ->value('buyer_user_id')
            ->add('foo', 'Search.Callback', [
                'callback' => function (\Cake\ORM\Query $query, array $args, \Search\Model\Filter\Base $filter) {
                }
            ]);

        $this->setTable('products');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Categories', [
            'foreignKey' => 'category_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'buyer_user_id',
        ]);
        $this->belongsToMany('Orders', [
            'through' => 'OrdersProducts',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        $validator
            ->decimal('price')
            ->requirePresence('price', 'create')
            ->notEmptyString('price');

        $validator
            ->scalar('description')
            ->requirePresence('description', 'create')
            ->notEmptyString('description');

        $validator
            ->dateTime('deleted')
            ->allowEmptyDateTime('deleted');

        $validator
            ->scalar('status')
            ->notEmptyString('status');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['category_id'], 'Categories'), ['errorField' => 'category_id']);
        $rules->add($rules->existsIn(['seller_user_id'], 'Users'), ['errorField' => 'seller_user_id']);
        $rules->add($rules->existsIn(['buyer_user_id'], 'Users'), ['errorField' => 'buyer_user_id']);

        return $rules;
    }

    public function setProductsSold(array $productIds, $userId)
    {
        return $this->query()->update()
            ->set(['status'=>'Sold','buyer_user_id'=>$userId])
            ->where(function (QueryExpression $exp,Query $q) use ($productIds){return $exp->in('id',$productIds);})
            ->execute();
    }

    public function editProduct($productId,$requestData,$userId)
    {
        $product = $this->get($productId);
        if (($product->status==='Active')&&($product->seller_user_id===$userId)) {
            $product = $this->patchEntity($product, $requestData, ['name', 'price', 'description', 'category_id']);
            if (!$this->save($product)) {
                throw new BadRequestException('Not changed');
            }
        }
        else throw new BadRequestException('It\'s sold or You are not owner');
        return $product;
    }

    public function delProduct($productId,$userId)
    {
        $product = $this->get($productId);
        if (!$product) throw new BadRequestException('asd');
        if (($product->status==='Active')&&($product->seller_user_id===$userId)) {
            if (!$product = $this->delete($product)) {
                throw new BadRequestException('Not deleted');
            }
        }
        else throw new BadRequestException('It\'s sold or You are not owner');
    }

    public function addProduct($requestData,$userId)
    {
        $product = $this->newEntity($requestData);
        $product->seller_user_id=$userId;
        if (!$this->save($product)) {
            throw new BadRequestException('Not added');
        }
        return $product;
    }
}
