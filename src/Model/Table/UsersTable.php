<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\Http\Exception\BadRequestException;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Users Model
 *
 * @property \App\Model\Table\HistoriesTable&\Cake\ORM\Association\HasMany $Histories
 * @property \App\Model\Table\OrdersTable&\Cake\ORM\Association\HasMany $Orders
 *
 * @method \App\Model\Entity\User newEmptyEntity()
 * @method \App\Model\Entity\User newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\User[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\User get($primaryKey, $options = [])
 * @method \App\Model\Entity\User findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\User patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\User[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\User|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class UsersTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('users');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('Histories', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('Orders', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('SellerUser', [
            'classname' => 'Products'
        ])->setForeignKey('seller_user_id');

        $this->hasMany('BuyerUser', [
            'classname' => 'Products'
        ])->setForeignKey('buyer_user_id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->email('email')
            ->requirePresence('email', 'create')
            ->notEmptyString('email');

        $validator
            ->scalar('password')
            ->requirePresence('password', 'create')
            ->notEmptyString('password');

        $validator
            ->scalar('first_name')
            ->requirePresence('first_name', 'create')
            ->notEmptyString('first_name');

        $validator
            ->scalar('last_name')
            ->requirePresence('last_name', 'create')
            ->notEmptyString('last_name');

        $validator
            ->decimal('amount')
            ->notEmptyString('amount');

        $validator
            ->dateTime('deleted')
            ->allowEmptyDateTime('deleted');

        $validator
            ->dateTime('birthday')
            ->requirePresence('birthday', 'create')
            ->notEmptyDateTime('birthday');

        $validator
            ->scalar('permissions')
            ->notEmptyString('permissions');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->isUnique(['email']), ['errorField' => 'email']);

        return $rules;
    }

    public function delUser($userId)
    {
        if ($this->query()->update()->set(['deleted' => time()])->where(['id' => $userId, 'deleted is ' => null])->execute()) {
            $Products = new ProductsTable();
            if (!$Products->query()->delete('Products')->where(['seller_user_id' => $userId, 'status' => 'Active'])->execute()) {
                throw new BadRequestException('Products are not deleted');
            }
        }
        else throw new BadRequestException('User not deleted');
    }

    public function changeAmount($sum,$userId)
    {
        $user = $this->get($userId);
        $amount=round(floatval($user->amount)+floatval($sum),2);
        if ($amount<0)
            throw new BadRequestException('Not enough money');
        $user->setAccess('amount',true);
        $user->set('amount',$amount);
        if (!$this->save($user)) {
            throw new BadRequestException('Not changed');
        }
        return $user;
    }

    public function addAmount($amount,$userId)
    {
        $user=$this->changeAmount($amount,$userId);
        $this->Histories->addHistory('REPLENISHMENT',$userId,null,$amount);
        return $user;
    }
    public function editUser($requestData,$userId)
    {
        $user = $this->get($userId);
        $user->setAccess('password', true);
        $user = $this->patchEntity($user, $requestData);
        if (!$this->save($user)) {
            throw new BadRequestException('Not edited');
        }
        return $user;
    }
    public function addUser($requestData)
    {
        $user = $this->newEmptyEntity();
        $user->setAccess(['email','password'],true);
        $user = $this->patchEntity($user, $requestData);
        if (!$this->save($user)) {
            throw new BadRequestException('Not added');
        }
        return $user;
    }
}
