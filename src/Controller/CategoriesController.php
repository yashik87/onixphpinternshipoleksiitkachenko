<?php
declare(strict_types=1);

namespace App\Controller;

use Cake\Core\Configure;
use Cake\Http\Exception\BadRequestException;

/**
 * Categories Controller
 *
 * @property \App\Model\Table\CategoriesTable $Categories
 * @method \App\Model\Entity\Category[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class CategoriesController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $categories = $this->paginate($this->Authorization->applyScope($this->Categories->find()));
        $this->set(compact('categories'));
        $this->set('_serialize','categories');
    }


    /**
     * View method
     *
     * @param string|null $id Category id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $category = $this->Categories->get($id);
        $this->Authorization->authorize($category);
        $this->set(compact('category'));
        $this->set('_serialize','category');
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $category = $this->Categories->patchEntity($this->Categories->newEmptyEntity(), $this->request->getData());
        $this->Authorization->authorize($category);
        if (!$this->Categories->save($category)) {
            throw new BadRequestException('Category is not added');
        }
        $this->set(compact('category'));
        $this->set('_serialize','category');
    }

    /**
     * Edit method
     *
     * @param string|null $id Category id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $category = $this->Categories->get($id);
        $this->Authorization->authorize($category);
        $category = $this->Categories->patchEntity($category, $this->request->getData());
        if (!$this->Categories->save($category)) {
            throw new BadRequestException('Category is not changed');
        }
        $this->set(compact('category'));
        $this->set('_serialize','category');
    }

    /**
     * Delete method
     *
     * @param string|null $id Category id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $category = $this->Categories->get($id);
        $this->Authorization->authorize($category);
        $this->Categories->delCat($id);
        $result = '';
        $this->set(compact('result'));
        $this->set('_serialize','result');
    }
}
