<?php
declare(strict_types=1);

namespace App\Controller;

use Cake\Core\Configure;

/**
 * Histories Controller
 *
 * @property \App\Model\Table\HistoriesTable $Histories
 * @method \App\Model\Entity\History[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class HistoriesController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $history = $this->paginate($this->Histories->find('Histories',['userId'=>$this->Authentication->getIdentity()->getIdentifier()]));

        $this->set(compact('history'));
        $this->set('_serialize','history');
    }

}
