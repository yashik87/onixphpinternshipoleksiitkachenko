<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\UsersTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\UsersTable Test Case
 */
class UsersTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\UsersTable
     */
    protected $Users;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.Users',
        'app.Histories',
        'app.Orders',
        'app.Products',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('Users') ? [] : ['className' => UsersTable::class];
        $this->Users = $this->getTableLocator()->get('Users', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Users);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delUser method
     *
     * @return void
     */
    public function testDelUser(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test changeAmount method
     *
     * @return void
     */
    public function testChangeAmount(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test addAmount method
     *
     * @return void
     */
    public function testAddAmount(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test editUser method
     *
     * @return void
     */
    public function testEditUser(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test addUser method
     *
     * @return void
     */
    public function testAddUser(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
