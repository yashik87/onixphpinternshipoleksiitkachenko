<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\OpsTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\OpsTable Test Case
 */
class OpsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\OpsTable
     */
    protected $Ops;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.Ops',
        'app.Orders',
        'app.Products',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('Ops') ? [] : ['className' => OpsTable::class];
        $this->Ops = $this->getTableLocator()->get('Ops', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Ops);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
