<?php
namespace lesson2;

include('lesson1.php');

use lesson1;

class User
{
//	private $name;
//	private $balance;
//	
//	function __construct($n,$b=0)
//	{
//		$this->name=$n;
//		$this->balance=$b;
//	}
//	
//	private function setName($n)
//	{
//		$this->name=$n;
//	}
//	public function getName()
//	{
//		return $this->name;
//	}
//	
//	protected function setBalance($b)
//	{
//		$this->balance=$b;
//	}
//	private function getBalance()
//	{
//		return $this->balance;
//	}
//
//	function __toString()
//	{
//		return "У пользователя ".$this->getName()." сейчас на счету $".$this->getBalance();
//	}
//	
//	function giveMoney($komu, $amount)
//	{
//		if ($this->getBalance()>=$amount)
//		{
//			$komu->setBalance($komu->getBalance()+$amount);
//			$this->setBalance($this->getBalance()-$amount);
//			return "Пользователь ".$this->getName()." перечислил $".$amount." пользователю ".$komu->getName();
//		}
//		else
//		{
//			return "У Вас недостаточно средств!!! Попробуйте перевести суму по меньше...";
//		}
//	}
}

abstract class Product
{
	private $name;
	private $price;
	private $owner;
	private static $products=[];
	
	function getall()
	{
		var_dump(self::$products);
	}
	
	private static function registerProduct(Product $prod)
	{
		if (!in_array($prod,self::$products,true))
		{
			self::$products[]=$prod; 
			return true;
		}
		else
			return false;
	}
	
	protected function __construct($name,$price,$owner)
	{
		$this->name=$name;
		$this->price=$price;
		$this->owner=$owner;
		self::registerProduct($this);
	}

	public static function iterat()
	{
		return new class(self::$products) implements \Iterator
		{
			public $s;
			public $position;
			public $arrray=[];
			
			function __construct($arr)
			{
				$this->array=$arr;
				$this->position=0;
			}
			
			function rewind()
			{
				$this->position=0;
			}
			
		    function current()
			{
				return $this->array[$this->position];
			}

			function key() 
			{
				return $this->position;
			}

			function next() 
			{
				++$this->position;
			}

			function valid()
			{
				return isset($this->array[$this->position]);
			}
			
		};

	}
	
	function __toString()
	{
		return $this->name.' '.$this->price.' '.$this->owner->getName().PHP_EOL;
	}

}

class Processor extends Product
{
	private $frequency;
	
	function __construct($name,$price,$owner,$frequency)
	{
		parent::__construct($name,$price,$owner);
		$this->frequency=$frequency;
	}
}

class Ram extends Product
{
	private $type;
	private $memory;

	function __construct($name,$price,$owner,$type,$memory)
	{
		parent::__construct($name,$price,$owner);
		$this->type=$type;
		$this->memory=$memory;
	}
}
//=============================================== ТЕСТЫ ===================================
//$user3= new lesson1\User('User3',35);
//$user4= new lesson1\User('User4',45);
//
//$product = new Processor('AMD Athlon',345.62,$user3,3500);
//$product0 = new Processor('AMD Athlon',345.62,$user4,3500);
//$product1 = new Processor('AMD Sempron',121,$user3,3500);
//$product2 = new Ram('DDR',180,$user4,'DDR 3','4 GB');
//$product3 = new Ram('DIMM',18,$user3,'SODIMM','128 MB');
//
//echo $product->registerProduct($product) ? 'Добавлен<br />' : 'Не добавлен<br />';
//echo $product->registerProduct($product) ? 'Добавлен<br />' : 'Не добавлен<br />';
//echo $product->registerProduct($product0) ? 'Добавлен<br />' : 'Не добавлен<br />';
//echo $product->registerProduct($product1) ? 'Добавлен<br />' : 'Не добавлен<br />';
//echo $product->registerProduct($product2) ? 'Добавлен<br />' : 'Не добавлен<br />';
//echo $product->registerProduct($product3) ? 'Добавлен<br />' : 'Не добавлен<br />';
//
//$a=$product::iterat();
//foreach($a as $value)
//	echo $value;
//
//$product->getall();
?>