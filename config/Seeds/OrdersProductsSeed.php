<?php
declare(strict_types=1);

use Migrations\AbstractSeed;

/**
 * OrdersProducts seed.
 */
class OrdersProductsSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => 17,
                'order_id' => 8,
                'product_id' => 7,
                'created' => '2020-10-30 01:11:51.930924',
                'modified' => '2020-10-30 01:11:51.931042',
            ],
            [
                'id' => 18,
                'order_id' => 8,
                'product_id' => 6,
                'created' => '2020-10-30 01:12:01.915153',
                'modified' => '2020-10-30 01:12:01.915237',
            ],
        ];

        $table = $this->table('orders_products');
        $table->insert($data)->save();
    }
}
