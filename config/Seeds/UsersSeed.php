<?php
declare(strict_types=1);

use Migrations\AbstractSeed;

/**
 * Users seed.
 */
class UsersSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => 2,
                'email' => 'qwe@qwe.qwe',
                'password' => 'qwe',
                'first_name' => 'qwe',
                'last_name' => 'qweqwe',
                'amount' => '0',
                'created' => '2020-09-30 20:40:38.397522',
                'modified' => '2020-09-30 20:56:26.802335',
                'deleted' => NULL,
                'birthday' => '1970-01-01 03:33:28',
                'permissions' => 'User',
            ],
            [
                'id' => 9,
                'email' => 'qwezxc@zxcasd.zxc',
                'password' => '$2y$10$t7vp5arAI.9lQ5TsSHtRwediGU66F8.iDrE9/JU2LZL.VjPjDKA8y',
                'first_name' => 'zxc',
                'last_name' => 'zxczxc',
                'amount' => '0',
                'created' => '2020-10-29 21:07:07.478403',
                'modified' => '2020-10-29 21:07:07.478491',
                'deleted' => '2020-10-30 02:59:54',
                'birthday' => '2010-01-01 00:00:01',
                'permissions' => 'User',
            ],
            [
                'id' => -1000,
                'email' => 'site@site.site',
                'password' => 'site',
                'first_name' => 'THIS',
                'last_name' => 'SITE',
                'amount' => '377.80',
                'created' => '2020-10-06 22:23:48.644948',
                'modified' => '2020-10-25 12:17:13.565253',
                'deleted' => NULL,
                'birthday' => '2020-01-01 00:00:00',
                'permissions' => 'Admin',
            ],
            [
                'id' => 5,
                'email' => 'zxc@zxc.zxc',
                'password' => '$2y$10$8uB1fxJ7l4Uy5t/o54en7OhevdGovD27rn13EDGYpx3fyT6Wk9B3G',
                'first_name' => 'zxc',
                'last_name' => 'zxczxc',
                'amount' => '0',
                'created' => '2020-10-05 01:15:02.431419',
                'modified' => '2020-10-30 02:43:12.886388',
                'deleted' => '2020-10-05 01:16:09',
                'birthday' => '2010-01-01 00:00:01',
                'permissions' => 'User',
            ],
            [
                'id' => 8,
                'email' => 'zxc@zxc.zxc',
                'password' => '$2y$10$0fNGacnim2f.AIS29Xfr4OypmrYrSsB/SfRuEDnofJq980.tEkOr.',
                'first_name' => 'zxc',
                'last_name' => 'zxczxc',
                'amount' => '0',
                'created' => '2020-10-25 08:41:02.324678',
                'modified' => '2020-10-25 08:41:02.32476',
                'deleted' => '2020-10-30 03:01:03',
                'birthday' => '2010-01-01 00:00:01',
                'permissions' => 'User',
            ],
            [
                'id' => 1,
                'email' => 'asd@asd.asd',
                'password' => '$2y$10$mnVCaXSOBclHdqWiKiGPUOnGFh0G80XB23qnhX2jMdsfnCc5A41.6',
                'first_name' => 'asd',
                'last_name' => 'asdasd',
                'amount' => '35843.92',
                'created' => '2020-09-29 23:23:53.398402',
                'modified' => '2020-10-30 03:54:42.97306',
                'deleted' => NULL,
                'birthday' => '1970-01-01 03:33:28',
                'permissions' => 'User',
            ],
        ];

        $table = $this->table('users');
        $table->insert($data)->save();
    }
}
