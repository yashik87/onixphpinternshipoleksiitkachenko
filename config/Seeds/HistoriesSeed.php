<?php
declare(strict_types=1);

use Migrations\AbstractSeed;

/**
 * Histories seed.
 */
class HistoriesSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => 12,
                'user_id' => 1,
                'order_id' => NULL,
                'action' => 'REPLENISHMENT',
                'suma' => '150.12',
                'created' => '2020-10-29 21:08:22.15862',
            ],
            [
                'id' => 13,
                'user_id' => 1,
                'order_id' => 8,
                'action' => 'SELL',
                'suma' => '1067.382',
                'created' => '2020-10-29 22:48:48.143503',
            ],
            [
                'id' => 14,
                'user_id' => 1,
                'order_id' => 8,
                'action' => 'SELL',
                'suma' => '117.382',
                'created' => '2020-10-29 22:48:48.188753',
            ],
            [
                'id' => 15,
                'user_id' => 1,
                'order_id' => 8,
                'action' => 'SELL',
                'suma' => '117.382',
                'created' => '2020-10-29 22:48:48.194819',
            ],
            [
                'id' => 16,
                'user_id' => -1000,
                'order_id' => 8,
                'action' => 'EARNINGS',
                'suma' => '372.2835',
                'created' => '2020-10-29 22:48:48.2048',
            ],
            [
                'id' => 17,
                'user_id' => 1,
                'order_id' => 8,
                'action' => 'BUY',
                'suma' => '7445.67',
                'created' => '2020-10-29 22:48:48.211394',
            ],
            [
                'id' => 26,
                'user_id' => 1,
                'order_id' => 8,
                'action' => 'SELL',
                'suma' => '117.38',
                'created' => '2020-10-30 01:14:18.955589',
            ],
            [
                'id' => 27,
                'user_id' => 1,
                'order_id' => 8,
                'action' => 'SELL',
                'suma' => '117.38',
                'created' => '2020-10-30 01:14:18.962721',
            ],
            [
                'id' => 28,
                'user_id' => -1000,
                'order_id' => 8,
                'action' => 'EARNINGS',
                'suma' => '12.36',
                'created' => '2020-10-30 01:14:18.970731',
            ],
            [
                'id' => 29,
                'user_id' => 1,
                'order_id' => 8,
                'action' => 'BUY',
                'suma' => '-247.12',
                'created' => '2020-10-30 01:14:18.987226',
            ],
            [
                'id' => 30,
                'user_id' => 1,
                'order_id' => NULL,
                'action' => 'REPLENISHMENT',
                'suma' => '150.12',
                'created' => '2020-10-30 01:23:31.992925',
            ],
            [
                'id' => 31,
                'user_id' => 1,
                'order_id' => NULL,
                'action' => 'REPLENISHMENT',
                'suma' => '150.12',
                'created' => '2020-10-30 03:03:46.332088',
            ],
            [
                'id' => 32,
                'user_id' => 5,
                'order_id' => NULL,
                'action' => 'REPLENISHMENT',
                'suma' => '150.12',
                'created' => '2020-10-30 03:43:05.834177',
            ],
            [
                'id' => 33,
                'user_id' => 5,
                'order_id' => NULL,
                'action' => 'REPLENISHMENT',
                'suma' => '150.12',
                'created' => '2020-10-30 03:43:26.529825',
            ],
            [
                'id' => 34,
                'user_id' => 5,
                'order_id' => NULL,
                'action' => 'REPLENISHMENT',
                'suma' => '150.12',
                'created' => '2020-10-30 03:44:24.007508',
            ],
            [
                'id' => 35,
                'user_id' => 5,
                'order_id' => NULL,
                'action' => 'REPLENISHMENT',
                'suma' => '150.12',
                'created' => '2020-10-30 03:44:39.019937',
            ],
            [
                'id' => 36,
                'user_id' => 5,
                'order_id' => NULL,
                'action' => 'REPLENISHMENT',
                'suma' => '150.12',
                'created' => '2020-10-30 03:46:35.581654',
            ],
            [
                'id' => 37,
                'user_id' => 5,
                'order_id' => NULL,
                'action' => 'REPLENISHMENT',
                'suma' => '150.12',
                'created' => '2020-10-30 03:47:46.300611',
            ],
            [
                'id' => 38,
                'user_id' => 1,
                'order_id' => NULL,
                'action' => 'REPLENISHMENT',
                'suma' => '150.12',
                'created' => '2020-10-30 03:49:14.032746',
            ],
            [
                'id' => 39,
                'user_id' => 1,
                'order_id' => NULL,
                'action' => 'REPLENISHMENT',
                'suma' => '150.12',
                'created' => '2020-10-30 03:49:19.00561',
            ],
            [
                'id' => 40,
                'user_id' => 1,
                'order_id' => NULL,
                'action' => 'REPLENISHMENT',
                'suma' => '150.12',
                'created' => '2020-10-30 03:49:20.937988',
            ],
            [
                'id' => 41,
                'user_id' => 1,
                'order_id' => NULL,
                'action' => 'REPLENISHMENT',
                'suma' => '150.12',
                'created' => '2020-10-30 03:49:46.239615',
            ],
            [
                'id' => 42,
                'user_id' => 1,
                'order_id' => NULL,
                'action' => 'REPLENISHMENT',
                'suma' => '150.12',
                'created' => '2020-10-30 03:54:11.910453',
            ],
            [
                'id' => 43,
                'user_id' => 1,
                'order_id' => NULL,
                'action' => 'REPLENISHMENT',
                'suma' => '150.12',
                'created' => '2020-10-30 03:54:13.922325',
            ],
            [
                'id' => 44,
                'user_id' => 1,
                'order_id' => NULL,
                'action' => 'REPLENISHMENT',
                'suma' => '150.12',
                'created' => '2020-10-30 03:54:15.950119',
            ],
            [
                'id' => 45,
                'user_id' => 1,
                'order_id' => NULL,
                'action' => 'REPLENISHMENT',
                'suma' => '150.12',
                'created' => '2020-10-30 03:54:17.689058',
            ],
            [
                'id' => 46,
                'user_id' => 1,
                'order_id' => NULL,
                'action' => 'REPLENISHMENT',
                'suma' => '150.12',
                'created' => '2020-10-30 03:54:39.413867',
            ],
            [
                'id' => 47,
                'user_id' => 1,
                'order_id' => NULL,
                'action' => 'REPLENISHMENT',
                'suma' => '150.12',
                'created' => '2020-10-30 03:54:41.464357',
            ],
            [
                'id' => 48,
                'user_id' => 1,
                'order_id' => NULL,
                'action' => 'REPLENISHMENT',
                'suma' => '150.12',
                'created' => '2020-10-30 03:54:42.99737',
            ],
        ];

        $table = $this->table('histories');
        $table->insert($data)->save();
    }
}
