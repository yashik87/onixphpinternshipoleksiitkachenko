<?php
declare(strict_types=1);

use Migrations\AbstractSeed;

/**
 * Products seed.
 */
class ProductsSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => 6,
                'category_id' => 4,
                'name' => 'p_qwe',
                'price' => '123.56',
                'description' => 'product asd',
                'created' => '2020-10-29 21:13:06.55728',
                'modified' => '2020-10-29 21:13:06.557361',
                'deleted' => NULL,
                'status' => 'Sold',
                'seller_user_id' => 1,
                'buyer_user_id' => 1,
            ],
            [
                'id' => 7,
                'category_id' => 4,
                'name' => 'p_qwe',
                'price' => '123.56',
                'description' => 'product asd',
                'created' => '2020-10-29 21:13:20.648018',
                'modified' => '2020-10-29 21:13:20.648103',
                'deleted' => NULL,
                'status' => 'Sold',
                'seller_user_id' => 1,
                'buyer_user_id' => 1,
            ],
        ];

        $table = $this->table('products');
        $table->insert($data)->save();
    }
}
