<?php
declare(strict_types=1);

use Migrations\AbstractSeed;

/**
 * Categories seed.
 */
class CategoriesSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => 4,
                'name' => 'Детские товары',
                'created' => '2020-09-27 01:37:14.253224',
                'modified' => NULL,
                'deleted' => NULL,
            ],
            [
                'id' => 5,
                'name' => 'asd',
                'created' => '2020-09-27 09:26:09.701065',
                'modified' => '2020-09-27 09:26:09.7014',
                'deleted' => NULL,
            ],
            [
                'id' => 1,
                'name' => 'qwe',
                'created' => '2020-09-27 01:37:14.253224',
                'modified' => '2020-09-29 02:40:08.340551',
                'deleted' => NULL,
            ],
            [
                'id' => 6,
                'name' => 'qwe',
                'created' => '2020-09-29 00:51:14.783247',
                'modified' => '2020-09-29 00:51:43.836051',
                'deleted' => NULL,
            ],
            [
                'id' => 2,
                'name' => 'Бензин',
                'created' => '2020-09-27 01:37:14.253224',
                'modified' => NULL,
                'deleted' => NULL,
            ],
            [
                'id' => 3,
                'name' => 'Садовый инвентарь',
                'created' => '2020-09-27 01:37:14.253224',
                'modified' => NULL,
                'deleted' => '2020-09-30 22:19:29',
            ],
            [
                'id' => 7,
                'name' => 'zxczxczxc',
                'created' => '2020-10-02 23:27:10.573127',
                'modified' => '2020-10-02 23:30:21.632501',
                'deleted' => '2020-10-02 23:32:34',
            ],
            [
                'id' => 8,
                'name' => 'qwe',
                'created' => '2020-10-13 10:38:37.445796',
                'modified' => '2020-10-13 10:39:44.970345',
                'deleted' => '2020-10-13 10:39:55',
            ],
            [
                'id' => 9,
                'name' => 'qwezxcasd',
                'created' => '2020-10-25 08:22:23.313361',
                'modified' => '2020-10-25 08:22:38.207787',
                'deleted' => '2020-10-25 08:22:46',
            ],
            [
                'id' => 11,
                'name' => 'asd',
                'created' => '2020-10-29 20:52:53.981882',
                'modified' => '2020-10-29 20:52:53.982',
                'deleted' => NULL,
            ],
            [
                'id' => 10,
                'name' => 'asd',
                'created' => '2020-10-29 18:58:23.88437',
                'modified' => '2020-10-29 18:58:23.884475',
                'deleted' => '2020-10-29 20:53:23',
            ],
        ];

        $table = $this->table('categories');
        $table->insert($data)->save();
    }
}
