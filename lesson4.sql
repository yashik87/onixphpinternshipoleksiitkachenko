create user intern with password 'asdasd';
create database market;
grant all privileges on database market to intern;

create table users 
(
	id serial PRIMARY KEY,
	name varchar(255) not null,
	amount money not null default 0
);



create table products
(
	id serial PRIMARY KEY,
	name varchar(255) not null,
	price money not null default 0,
	owner integer not null,
	kind varchar(30) not null, -- PROC, RAM � �������� ������(����� � int, �� ����� � php ����� ����� ������������� ������������)
	type varchar(30) null, 
	memory integer,
	frequency integer,
	
	foreign key (owner) references users(id) on delete cascade on update cascade
);



insert into users (name,amount) values('User1',500);
insert into users (name,amount) values('User2',250);
insert into users (name,amount) values('User3',950);



insert into products (name,price, owner, kind, frequency) values('AMD ATHLON',235.41,1,'PROC',3000);
insert into products (name,price, owner, kind, frequency) values('AMD ATHLONx9',1890,1,'PROC',32000);
insert into products (name,price, owner, kind, frequency) values('Intel Celeron',156.23,2,'PROC',1000);
insert into products (name,price, owner, kind, frequency) values('Intel Pentium',259,2,'PROC',4000);
insert into products (name,price, owner, kind, frequency) values('ATOM',56.89,3,'PROC',750);
insert into products (name,price, owner, kind, type, memory) values('Hyunix',10,1,'RAM','DDR2',4);
insert into products (name,price, owner, kind, type, memory) values('Hyunix',12,2,'RAM','DDR2',4);
insert into products (name,price, owner, kind, type, memory) values('Hyunix',4,3,'RAM','DIMM',256);




select users.name as owner, products.name as product 
from users left join products on users.id=products.owner;
-- ���(���� ������ ������������ � �������)
/*select users.name, products.name from users, products
where users.id=products.owner;*/




update products set owner = 2 where id = 1;
--update products set owner = 2 where owner = 3;



delete from users where id = 3;



select users.name as owners, count(products.id) as counts
from users, products
where users.id=products.owner group by owners;




alter table users 
	add column email varchar(255) unique,
	add column birthday date check(birthday<current_date),
	add column age integer;



	
create or replace function make_age() returns trigger as $$
begin
NEW.age=date_part('year',age(NEW.birthday::timestamp);
return NEW;
end $$ language plpgsql;




create trigger user_age before insert or update on users for each row execute procedure make_age();



create sequence incrementIdUser increment by 1 start with 10000;



alter table users alter column id type integer, alter column id set default nextval('incrementIdUser');



begin;
insert into users (name,amount) values('User4',500);
insert into products (name,price, owner, kind, type, memory) values('Hyunix',12,2,'RAM','DDR2',4);
insert into products (name,price, owner, kind, type, memory) values('Hyunix',4,3,'RAM','DIMM',256);
insert into products (name,price, owner, type, memory) values('Hyunix',4,3,'DIMM',256);
commit;


