<?php
namespace lesson3;

include('lesson2.php');

use lesson2,lesson1;

class User
{
	private $name;
	private $balance;
	
	function __construct($n,$b=0)
	{
		$this->name=$n;
		$this->balance=$b;
	}
	
	private function setName($n)
	{
		$this->name=$n;
	}
	public function getName()
	{
		return $this->name;
	}
	
	protected function setBalance($b)
	{
		$this->balance=$b;
	}
	public function getBalance()
	{
		return $this->balance;
	}

	function __toString()
	{
		return "У пользователя ".$this->getName()." сейчас на счету $".$this->getBalance();
	}
	
	function giveMoney($komu, $amount)
	{
		if ($this->getBalance()>=$amount)
		{
			$komu->setBalance($komu->getBalance()+$amount);
			$this->setBalance($this->getBalance()-$amount);
			return "Пользователь ".$this->getName()." перечислил $".$amount." пользователю ".$komu->getName();
		}
		else
		{
			return "У Вас недостаточно средств!!! Попробуйте перевести суму по меньше...";
		}
	}
 	function listProduct()
	{
		return Product::getUserList($this);
	}
 	function sellProduct(Product $prod,User $user)
	{
		return Product::sellProduct($this,$user,$prod);
	}	
}

abstract class Product
{
	private $name;
	private $price;
	private $owner;
	private static $products=[];
	
	function getall()
	{
		var_dump(self::$products);
	}
	
	private static function registerProduct(Product $prod)
	{
		if (!in_array($prod,self::$products,true))
		{
			self::$products[]=$prod; 
		}
	}
	
	protected function __construct($name,$price,$owner)
	{
		$this->name=$name;
		$this->price=$price;
		$this->owner=$owner;
		self::registerProduct($this);
	}

	public static function iterat()
	{
		return new class(self::$products) implements \Iterator
		{
			public $s;
			public $position;
			public $arrray=[];
			
			function __construct($arr)
			{
				$this->array=$arr;
				$this->position=0;
			}
			
			function rewind()
			{
				$this->position=0;
			}
			
		    function current()
			{
				return $this->array[$this->position];
			}

			function key() 
			{
				return $this->position;
			}

			function next() 
			{
				++$this->position;
			}

			function valid()
			{
				return isset($this->array[$this->position]);
			}
			
		};

	}
	
	function __toString()
	{
		return $this->name.' '.$this->price.' '.$this->owner->getName().PHP_EOL;
	}
	public static function getUserList(User $user)
	{
		$s='';
		$mas=array_filter(self::$products,function($k) use ($user) {return $k->owner===$user;});
		foreach($mas as $value)
			$s.=$value;
		return $s;
	}

	public static function sellProduct(User $owner, User $user, Product $prod)
	{
		$s='';
		$mas=array_filter(self::$products,function($k) use ($owner,$prod) {return ($k->owner===$owner)&&($k===$prod);},ARRAY_FILTER_USE_BOTH);
		
		if (count($mas)>0)
		{
				if ($user->giveMoney($owner,$mas[key($mas)]->price))
				{
					self::$products[key($mas)]->owner=$user;
					return 'Пользователь '.$owner->getName().' продал продукт '.$prod->name.'('.$prod->price.') пользователю '.$user->getName().' так как имеет '.$user->getBalance();
				}
				else
				{
					return 'Пользователь '.$user->getName().' не может перечеслить '.$prod->name.'('.$prod->price.') пользователю '.$owner->getName().' так как имеет '.$user->getBalance();
				}
		}
		else 
			return 'Пользователь '.$user->getName().' не может продать '.$prod->name.'('.$prod->price.') так как он пренадлежит пользователю '.$owner->getName();

	}

	public static function randomProducts(User $user)
	{
		$r=rand(1,10);
		$name=rand(100,1000);
		if ($r<=5) {$randomProduct=new RAM('RAM'.$name,rand(100,999),$user,rand(1,64),'DIMM');}
		if ($r>5) {$randomProduct=new Processor('AMD'.$name,rand(100,999),$user,rand(1000,10000));}
		return $randomProduct;
	}
}

class Processor extends Product
{
	private $frequency;
	
	function __construct($name,$price,$owner,$frequency)
	{
		parent::__construct($name,$price,$owner);
		$this->frequency=$frequency;
	}
}

class Ram extends Product
{
	private $type;
	private $memory;

	function __construct($name,$price,$owner,$type,$memory)
	{
		parent::__construct($name,$price,$owner);
		$this->type=$type;
		$this->memory=$memory;
	}
}
//=============================================== ТЕСТЫ ===================================
$user3=new User('User3',35);
$user4=new User('User4',55);

$product0 = new Processor('AMD Athlon',60,$user4,3500);
$product1 = new Processor('AMD Sempron',11,$user3,3500);
$product2 = new Ram('DDR',18,$user4,'DDR 3','4 GB');
$product3 = new Ram('DIMM',80,$user3,'SODIMM','128 MB');
$product4 = new Processor('AMD Athlon',50,$user3,3500);

$product5=RAM::randomProducts($user3);
$product6=RAM::randomProducts($user3);
$product7=RAM::randomProducts($user4);
$product8=RAM::randomProducts($user3);


$a=$product0::iterat();
foreach($a as $value)
	echo $value;


echo PHP_EOL.$user3->listProduct();
echo PHP_EOL.$user4->listProduct();
//
//                                                сумы денег 
//                                                 на счету 
//                                              у пользователей
//													  ||
//													  \/ 
echo PHP_EOL.$user4->sellProduct($product2,$user3);//17/73
echo PHP_EOL.$user3->sellProduct($product4,$user4);//67/23
echo PHP_EOL.$user3->sellProduct($product4,$user4);
echo PHP_EOL.$user4->sellProduct($product4,$user3);//17/73
echo PHP_EOL.$user4->sellProduct($product0,$user3);
//
echo PHP_EOL.$user3;
echo PHP_EOL.$user4;
//
echo PHP_EOL.$user3->listProduct();
echo PHP_EOL.$user4->listProduct();


?>